import React from 'react';
import {Accordion, AccordionItem} from 'carbon-components-react';

export default class Projects extends React.Component {  
    constructor(props){
        super(props);
    }

    componentDidMount(){}

    componentWillMount(){}

    render() {
      return (
        <div className="bx--col-xs-9 bx--col-md-9">
            <Accordion>
                {this.props.projects.map( (project) =>
                <AccordionItem key={project.id}  title={project.title}>
                    <span>{project.description}</span>
                </AccordionItem>
                )}
            </Accordion>
        </div>  
      );
    }
  }