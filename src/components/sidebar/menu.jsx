import React from 'react';

export default class Menu extends React.Component {
    render() {
      return (
        <ul className="bx--list--unordered menu">
            <li className="bx--list__item"><a href="#" className="active">Projects</a></li>
            <li className="bx--list__item"><a href="#">Control Panel</a></li>
            <li className="bx--list__item"><a href="#">Settings</a></li>
            <li className="bx--list__item"><a href="#">Help</a></li>
            <li className="bx--list__item"><a href="#">Status</a></li>
            <li className="bx--list__item"><a href="#">About</a></li>
        </ul> 
      );
    }
  }