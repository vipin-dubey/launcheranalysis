import React from 'react';
import Logo from './logo';
import Title from './title';
import UserInfo from './userInfo';

export default class Header extends React.Component {
    render() {
      return (
        <div>
          <div className = "header bx--row">
                  <div className="bx--col-xs-2 bx--col-md-2"><Logo /></div>
                  <div className="bx--col-xs-8 bx--col-md-8"><Title /></div>
                  <div className="bx--col-xs-2 bx--col-md-2"><UserInfo /></div>      
          </div>  
        </div>  
      );
    }
  }