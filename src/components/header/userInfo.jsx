import React from 'react';

function formatName(user){
  return user.firstName + ' ' + user.lastName;
}

export default class UserInfo extends React.Component {

    constructor(props){
      super(props);
      this.state = {
        firstName:null,
        lastName:null,
      }
    }

    componentDidMount(){
      this.setState({
        firstName:'Vipin',
        lastName:'Dubey',
      })
    }

    componentWillMount(){

    }

    render() {
      return (
        <div className="user-info">
               <p>{formatName(this.state)} ...</p>
        </div>  
      );
    }
  }