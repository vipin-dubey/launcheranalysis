import React from 'react';
import Header from './components/header/header';
import Sidebar from './components/sidebar/sidebar';
import Projects from './components/projects/projectList';

const projects = [
  {
      id:1,
      title:'The weather channel - Hurricane',
      description:'secondary information line',
      syncProject:true,
      tags:['AR','VS','Frontier 2.0.1'],
      levels:[
          {
              id:1,
              title:'Level_Blackout_MarySue_version02_Final',
              description:'This level is the master level description of four others. Watchout for the proper setup of lightning effect. Remember that there are 2 different triggers for the day and night setup, which we have to be...',
              tags:['Master','AR'],
              favorite:true

          },
          {
              id:2,
              title:'Flooding the basement',
              description:'This level is the master level description of four others. Watchout for the proper setup of lightning effect.',
              tags:['VS'],
              favorite:false

          },
          {
              id:3,
              title:'Car drop - material test',
              description:'This level is the master level description of four others.',
              tags:['AR'],
              favorite:true

          }
      ]

  },
  {
      id:2,
      title:'Frontier suite - demo project',
      description:'secondary information line',
      syncProject:true,
      tags:['AR','VS','Frontier 2.0.1'],
      levels:[
          {
              id:1,
              title:'Level_Blackout_MarySue_version02_Final',
              description:'This level is the master level description of four others. Watchout for the proper setup of lightning effect. Remember that there are 2 different triggers for the day and night setup, which we have to be...',
              tags:['Master','AR'],
              favorite:true

          },
          {
              id:2,
              title:'Flooding the basement',
              description:'This level is the master level description of four others. Watchout for the proper setup of lightning effect.',
              tags:['VS'],
              favorite:false

          },
          {
              id:3,
              title:'Car drop - material test',
              description:'This level is the master level description of four others.',
              tags:['AR'],
              favorite:true

          }
      ]

  },{
      id:3,
      title:'Frontier suite - demo project - copy',
      description:'secondary information line',
      syncProject:true,
      tags:['AR','VS','Frontier 2.0.1'],
      levels:[
          {
              id:1,
              title:'Level_Blackout_MarySue_version02_Final',
              description:'This level is the master level description of four others. Watchout for the proper setup of lightning effect. Remember that there are 2 different triggers for the day and night setup, which we have to be...',
              tags:['Master','AR'],
              favorite:true

          },
          {
              id:2,
              title:'Flooding the basement',
              description:'This level is the master level description of four others. Watchout for the proper setup of lightning effect.',
              tags:['VS'],
              favorite:false

          },
          {
              id:3,
              title:'Car drop - material test',
              description:'This level is the master level description of four others.',
              tags:['AR'],
              favorite:true

          }
      ]

  }
];

export default class App extends React.Component {
  render() {
    return (
      <div className="bx--grid main-wrapper">
            <Header /> 
            <div className = "sidebar bx--row">
              <Sidebar />
              <Projects projects = {projects}/>
            </div>  
      </div> 
    );
  }
}
